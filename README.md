

## DeviceAdminApp

Esta aplicación ha sido desarrollada por Beatriz González,
las herramientas que se utilizaron son las siguientes:

1. Java, Android nativo.
2. XML (Diseño de layout).
3. Android Studio.
4. Gestor de base de datos SQLite.

Descripción de aplicación: Utiliza las funciones de administrador de los dipositivos, borrar información del dispositivo,
reestablecer valor de fábrica del dispositivo y encriptar información.